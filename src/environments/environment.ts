// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBPG0jLL2jS6aVHitaN_j8iJ7q7a04pBqY",
    authDomain: "cancer-project-ecdcf.firebaseapp.com",
    projectId: "cancer-project-ecdcf",
    storageBucket: "cancer-project-ecdcf.appspot.com",
    messagingSenderId: "949011267929",
    appId: "1:949011267929:web:fa094e1732f1e90eb2cc48"
  } 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
